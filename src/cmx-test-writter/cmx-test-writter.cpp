#include <cstdlib>
#include <limits>

#include <cmw-cmx-cpp/ProcessComponent.h>
#include <cmw-cmx-cpp/Component.h>

using namespace cmw::cmx;

int main(int argc, char * argv[])
{
    ComponentPtr component = Component::create("test_comp_0");
    component->newInt64("test.int.value") = 666;
    component->newInt64("test.int_value.other") = 777;
    component->newFloat64("test.float.value1") = 2.0f;
    component->newFloat64("test.float.value2") = 2.1f;
    component->newFloat64("test.float.value3") = 2.2f;
    component->newFloat64("test.float.value4") = 2.3f;
    component->newBool("test.bool.value") = false;

    CmxInt64 counterInt1(component->newInt64("test.value1.counter"));
    counterInt1 = 0;

    CmxInt64 counterInt2(component->newInt64("test.value2.counter"));
    counterInt2 = 123;

    CmxInt64 random(component->newInt64("test.random"));

    component = Component::create("test_module");

    CmxInt64 module1Random(component->newInt64("test:module:1:random"));
    component->newInt64("test:module:1:version") = 123;

    CmxInt64 module2Random(component->newInt64("test:module:2:random"));
    component->newInt64("test:module:2:version") = 234;

    CmxInt64 module3Random(component->newInt64("test:module:3:random"));
    component->newInt64("test:module:3:version") = 345;

    component = Component::create("another");
    component->newFloat64("first.metric") = 567.88888f;
    component->newFloat64("second.metric") = 2391847.4543f;
    component->newInt64("simple.int") = 1234567890;

    CmxInt64 counterInt3(component->newInt64("int.counter"));
    counterInt3 = 0;

    CmxFloat64 counterFloat1(component->newFloat64("float.counter"));
    counterFloat1 = 0;

    component = Component::create("min_max");
    component->newInt64("int64.min") = std::numeric_limits<int64_t>::min();
    component->newInt64("int64.max") = std::numeric_limits<int64_t>::max();
    component->newFloat64("double.min") = std::numeric_limits<double>::min();
    component->newFloat64("double.max") = std::numeric_limits<double>::max();
    component->newFloat64("double.nan") = std::numeric_limits<double>::quiet_NaN();
    component->newFloat64("double.negative_min") = -std::numeric_limits<double>::min();
    component->newFloat64("double.negative_max") = -std::numeric_limits<double>::max();
    component->newFloat64("double.negative_nan") = -std::numeric_limits<double>::quiet_NaN();

    while (true)
    {
        counterInt1 = counterInt1 + 1;
        counterInt2 = counterInt2 + 2;
        counterInt3 = counterInt3 + 1;
        counterFloat1 = counterFloat1 + 0.123f;
        random = rand() % 100;
        module1Random = rand() % 100;
        module2Random = rand() % 100;
        module2Random = rand() % 100;
        ProcessComponent::update();
        sleep(1);
    }
}
